=========================================
Programação Orientada a Objetos em Python
=========================================

Inspirado nas `notas de aula Computação II (MAB 225/240) <http://dcc.ufrj.br/~fabiom/java/>`_ do
professor `Fabio Mascarenhas <http://dcc.ufrj.br/~fabiom/>`_.

.. contents:: Conteúdo
   :local:

Introdução
==========

Este é um curso primariamente de orientação a objetos e não de Python 2. O objetivo é ensinar aos
alunos do ciclo básico de engenharia o essencial da programação orientada a objetos, tomando por
base seus conhecimentos prévios de programação procedural (MAB 114).

Naturalmente, ao escolher uma linguagem específica para implementar este projeto, temos que meio e
finalidade acabam por, inevitavelmente, andar de mãos dadas. Por isso, a porção final deste curso
aproveita a expansão da capacidade de programação em python dos alunos para introduzir algumas
ferramentas úteis à documentação e comunicação científicas oferecidas em python, sem perder de vista
a ênfase em orientação a objetos. As aulas finais do semestre serão, portanto, ocupadas por um
mini-curso denominado Python Científico, em que introduzimos as bibliotecas numpy, scipy e
matplotlib. Este mini-curso pode ser oferecido separadamente, logo está disponível em outro
repositório (link faltando).

Este curso, apresentado no primeiro semestre de 2015, cobre a versão 2.7 do Python, cujo ciclo
de vida `se encerrará completamente <https://pythonclock.org/>`_ a partir de 1º de janeiro de
2020. Embora eu deva ministrar novos cursos em versões mais modernas da linguagem (no momento em
que escrevo isso, a 3.8 está para sair do forno), resolvi manter este material, e o de outros
cursos ministrados no período de 2015-2016, em sua forma original, por razões históricas. 

.. important::

   Tenha em mente que, como realizei a curadoria do material apresentado aqui numa época posterior à
   apresentação do curso, o repositório utiliza um ferramental Python 3.5 para produzir estas
   páginas, apesar da versão abordada ser a 2.7. Este fato é importante caso você resolva baixar o
   repositório e explorar o material, conforme explicado em Instalando_.

Instruções
==========

Os arquivos de mídia (imagens, vídeo, etc.) são mantidos separadamente, na seção de downloads do
Bitbucket. Baixe `aqui <https://bitbucket.org/pedroasad/oo-python-course/downloads/midia.zip>`_ o
pacote com todos os arquivos e descompacte-o no diretório do projeto.

Plano do curso
==============

Ementa
------

Ementa [#ementa]_ do curso de Computação II (MAB 225):

* Introdução à programação orientada a objetos com a linguagem Python

  * Conceitos de classe, objeto, instância e encapsulamento

* Depuração e teste automático

  * PyUnit
  * pdb

* Interfaces gráficas (GUI)

  * TkInter

* Projeto de Software Orientado a Objetos na linguagem Python

  * Métodos recursivos
  * Polimorfismo
  * Interfaces
  * Padrões de projeto

* Pacotes para aplicações matemáticas e em engenharia

  * Manipulação de vetores, matrizes, operações de álgebra linear (numpy)
  * Plotagem de gráficos e aplicações gráficas (matplotlib)

* Tratamento de exceções
* Persistência de dados

  * Arquivos texto e arquivos binários
  * Serialização de objetos

.. [#ementa] Segundo o site do DCC: http://dcc.ufrj.br/~pythonufrj/ementas.html

Duração e distribuição de conteúdos
-----------------------------------

A duração prevista do curso é de 15 a 20 sessões teóricas e e 15 a 20 sessões práticas (em
laboratório). Os primeiros encontros podem ser dedicados a uma revisão dos conteúdos essenciais da
linguagem Python e da programação procedural. Os encontros imediatamente anteriores às provas podem
ser reservados à revisão dos conteúdos. Contar com alguns encontros reserva é uma boa maneira de
assegurar a cobertura dos tópicos essenciais e as aulas restantes podem ser utilzadas para
aprofundar tópicos opcionais. Sendo assim, sugere-se o seguinte plano de aulas teóricas:

* **Perfil da turma** (questionário para o início e final do curso)

  * Qual é o seu curso de graduação?
  * Qual é o seu período?
  * Há quanto tempo cursou Computação I?
  * O que achou de Computação I?
  * Com quais linguagens de programação você teve contatovanteriormente?
  * Já cursou Computação II anteriormente? O que achou difícil?
  * O que você espera deste curso? O que a programação pode lhe oferecer?

* **Aula 0 -** Revisão de Python procedural

  * Paradigma procedural
  * Funções
  * Tipos de dados básicos
  * Variáveis e escopo
  * Tipos compostos: tupla, string, lista e dicionário
  * Controle de fluxo: estruturas condicionais
  * Controle de fluxo: laços
  * Controle de fluxo: parada e desvio forçados

* **Aula 1 -** Introdução à programação orientada a objetos

  * Objetos e classes
  * Definindo classes em python
  * Construtores
  * Acessores (getters e setters)

* Classes e objetos

  * Métodos
  * Construtores e atributos
  * Acessores (getters e setters)

* Herança
* Sobrecarga de métodos

Quanto às aulas práticas, um formato sugerido é o de estudos dirigidos que abranjam o conteúdo das
aulas teóricas correspondentes. Neste formato, um pequeno roteiro introdutório é apresentado e
segue-se uma lista de exercícios de complexidade crescente, em que os conteúdos necessários são
apresentados gradualmente, acompanhando a complexidade dos exercícios. As últimas etapas podem não
incluir nenhuma orientação, demandando dos alunos domínio sobre os conteúdos anteriores e
possibilitando soluções únicas e criativas.

Dependendo do arranjo físico do laboratório, pode-se sugerir que os alunos sentem em dupla ou trio,
de maneira a encorajar a troca de informações entre os mesmos mas não favorecer demais a perda de
foco.

Instalando
==========

Autores
=======

* `Pedro Asad <mailto:pasad@cos.ufrj.br>`_
