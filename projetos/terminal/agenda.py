# coding: utf-8
from datetime import date, timedelta

class Agenda(dict):
    def __init__(self, nome, *args, **kwargs):
        dict.__init__(*args, **kwargs)
        self.nome = nome

    def atividades_entre(self, data_i, data_f):
        pass

    def proxima_atividade(self, datahora):
        pass

class Atividade:
    def __init__(self, prioridade = 'normal', categorias = [], regimes = []):
        self.prioridade = prioridade
        self.categorias = categorias
        self.regimes = regimes

    def horas_entre(self, data_i, data_f):
        pass

    def ocorrencia_seguinte(self, data):
        pass

    def ocorrencia_anterior(self, data):
        pass

class RegimeRepeticao:
    def anterior(self, data):
        return None

    def seguinte(self, data):
        return None

class RegimeUnico(RegimeRepeticao):
    def __init__(self, data, duracao):
        pass

class RegimeDiario(RegimeRepeticao):
    def __init__(self, data_inicial, duracao, data_final = None):
        pass

class RegimeSemanal(RegimeRepeticao):
    def __init__(self, data_inicial, duracao, data_final = None):
        pass

class RegimeMensal(RegimeRepeticao):
    def __init__(self, data_inicial, duracao, data_final = None):
        pass

agenda = Agenda('Agenda 2015')
agenda['Calculo I'] = Atividade('alta', categorias = ['estudo', 'ufrj'])
