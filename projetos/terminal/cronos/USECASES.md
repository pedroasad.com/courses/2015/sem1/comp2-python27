##Registro de atividade##
1. No painel principal, usuário seleciona a opção "O quê estou fazendo?"
1. Uma painel entitulado "Atividades recentes" mostra as atividades mais
recentes praticadas, que podem ser escolhidas para preencher as opções
1. Uma painel entitulado "Atividades rotineir
1. Preenche o conteúdo da caixa de texto entitulada "Título"
	1. A aplicação mostra sugestões de título com base em entradas similares já
	registradas
1. Preenche ou não o conteúdo da caixa de texto entitulada "Descrição"
	1. Se a caixa anterior foi preenchida com valor pré-existente, esta aqui
	deve ser completada com valor já armazenado, se existente
1. Um botão "Adicionar detalhes", se clicado, revela opções adicionais
1. Uma caixa de seleção de data e hora é 
