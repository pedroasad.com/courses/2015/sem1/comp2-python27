
Falou, tá falado
A minha gente hoje anda
E olhando pro chão, viu
Você que inventou esse estado
Toda a escuridão
Esqueceu-se de inventar

Amanhã há de ser
Eu pergunto a você
Da enorme euforia
Quando o galo insistir
Água nova brotando
Sem parar
Quando chegar o momento
Vou cobrar com juros, juro
Esse grito contido

Ora, tenha a fineza
Você vai pagar e é dobrado
Nesse meu penar
Apesar de você
Outro dia
O jardim florescer
Você vai se amargar
Sem lhe pedir licença
Que esse dia há de vir

Amanhã há de ser
Você vai ter que ver
E esbanjar poesia
Vendo o céu clarear
Como vai abafar
Na sua frente
Apesar de você
Outro dia
Etc. e tal

