# coding: utf-8
from calendario import *

def teste_ehBissexto():
	# Alguns destes anos são seculares não múltiplos de 400, porém anteriores a
	# 1582, logo bissextos pelo critério do calendário juliano. 
	assert ehBissexto(1400) == True
	assert ehBissexto(1500) == True
	assert ehBissexto(1600) == True
	assert ehBissexto(1700) == False

	# Para os anos abaixo, vale o critério gregoriano: anos seculares são
	# bissextos apenas se forem múltiplos de 400
	assert ehBissexto(1800) == False
	assert ehBissexto(1900) == False
	assert ehBissexto(2000) == True
	assert ehBissexto(2100) == False

	# Para os anos abaixo, é necessário e suficiente ser múltiplo de 4.	
	assert ehBissexto(1888) == True
	assert ehBissexto(1889) == False
	assert ehBissexto(1890) == False
	assert ehBissexto(1891) == False

def teste_diasNoMes():
	meses_curtos = { 4, 6, 9, 11 }
	meses_longos = { 1, 3, 5, 7, 8, 10, 12 }

	# 1582
	assert diasNoMes( 2, 1582) == 28
	assert diasNoMes(10, 1582) == 21
	for m in meses_curtos:
		assert diasNoMes(m, 1582) == 30
	for m in meses_longos - {10}:
		assert diasNoMes(m, 1582) == 31

	# 1400
	assert diasNoMes(2, 1400) == 29
	for m in meses_curtos:
		assert diasNoMes(m, 1400) == 30
	for m in meses_longos:
		assert diasNoMes(m, 1400) == 31

	# 1400
	assert diasNoMes(2, 1400) == 29
	for m in meses_curtos:
		assert diasNoMes(m, 1400) == 30
	for m in meses_longos:
		assert diasNoMes(m, 1400) == 31

	# 1500
	assert diasNoMes(2, 1500) == 29
	for m in meses_curtos:
		assert diasNoMes(m, 1500) == 30
	for m in meses_longos:
		assert diasNoMes(m, 1500) == 31

	# 1600
	assert diasNoMes(2, 1600) == 29
	for m in meses_curtos:
		assert diasNoMes(m, 1600) == 30
	for m in meses_longos:
		assert diasNoMes(m, 1600) == 31

	# 1700
	assert diasNoMes(2, 1700) == 28
	for m in meses_curtos:
		assert diasNoMes(m, 1700) == 30
	for m in meses_longos:
		assert diasNoMes(m, 1700) == 31

	# 1800
	assert diasNoMes(2, 1800) == 28
	for m in meses_curtos:
		assert diasNoMes(m, 1800) == 30
	for m in meses_longos:
		assert diasNoMes(m, 1800) == 31

	# 1900
	assert diasNoMes(2, 1900) == 28
	for m in meses_curtos:
		assert diasNoMes(m, 1900) == 30
	for m in meses_longos:
		assert diasNoMes(m, 1900) == 31

	# 2000
	assert diasNoMes(2, 2000) == 29
	for m in meses_curtos:
		assert diasNoMes(m, 2000) == 30
	for m in meses_longos:
		assert diasNoMes(m, 2000) == 31

	# 1886
	assert diasNoMes(2, 1886) == 28
	for m in meses_curtos:
		assert diasNoMes(m, 1886) == 30
	for m in meses_longos:
		assert diasNoMes(m, 1886) == 31

	# 1887
	assert diasNoMes(2, 1887) == 28
	for m in meses_curtos:
		assert diasNoMes(m, 1887) == 30
	for m in meses_longos:
		assert diasNoMes(m, 1887) == 31

	# 1888
	assert diasNoMes(2, 1888) == 29
	for m in meses_curtos:
		assert diasNoMes(m, 1888) == 30
	for m in meses_longos:
		assert diasNoMes(m, 1888) == 31

	# 1889
	assert diasNoMes(2, 1889) == 28
	for m in meses_curtos:
		assert diasNoMes(m, 1889) == 30
	for m in meses_longos:
		assert diasNoMes(m, 1889) == 31

def teste_diasAteOMes():
	dias_ate          = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334]
	dias_ate_bissexto = [0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335]
	dias_ate_1582     = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 294, 324]

	for ano in [1400, 1500, 1582, 1600, 1700, 1800, 1886, 1887, 1888, 1889,
1900, 2100]:
		if ano == 1582:
			duracoes = dias_ate_1582
		elif ehBissexto(ano):
			duracoes = dias_ate_bissexto
		else:
			duracoes = dias_ate

		for mes in range(1, 13):
			assert diasAteOMes(mes, ano) == duracoes[mes - 1]

def teste_diaDoAno():
	assert diaDoAno(1, 3, 2013) == 60
	assert diaDoAno(1, 3, 2014) == 60
	assert diaDoAno(1, 3, 2015) == 60
	assert diaDoAno(1, 3, 2016) == 61

def teste_diasEntre():
	assert diasEntre((17,  3, 2015), (14,  7, 2015)) == 119
	assert diasEntre((17,  3, 2015), (14,  7, 2016)) == 485

	assert diasEntre((25,  3, 2014), (25,  3, 2015)) == 365
	assert diasEntre((25,  3, 2014), (25,  3, 2016)) == 731

def teste_diaDaSemana():
	assert diaDaSemana(25,  3, 1987) == 4
	assert diaDaSemana(25,  3, 1988) == 6
	assert diaDaSemana(25,  3, 1989) == 7
	assert diaDaSemana(25,  3, 1990) == 1

	assert diaDaSemana(25,  3, 2014) == 3
	assert diaDaSemana(25,  3, 2015) == 4
	assert diaDaSemana(25,  3, 2016) == 6
	assert diaDaSemana(25,  3, 2017) == 7
