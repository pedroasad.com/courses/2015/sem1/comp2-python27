import cenario
import random as rd
from objetos import Bola

min_raio = 0.6
max_raio = 1.5
bolas = []

cores = [(200, 50, 50, 255), (50, 200, 50, 255), (50, 50, 200, 255), (200, 200, 50, 255), (200, 50, 200, 255), (50, 200, 200, 255)]

def setup():
    size(cenario.pixels(cenario.largura), cenario.pixels(cenario.altura))
    
def mouseClicked():
    global bolas
    
    explodidas = []
    
    for i in range(len(bolas)):
        if Bola.ao_redor_de(bolas[i], cenario.metros(mouseX),  cenario.metros(mouseY)):
            explodidas = [i] + explodidas
    
    for i in explodidas:
        bolas += bolas[i].explodir()
        del bolas[i]
        
    if not explodidas:
        b = Bola(
            'Bola %d' % (len(bolas) + 1),
            random(min_raio, max_raio),
            rd.choice(cores),
            (cenario.metros(mouseX), cenario.metros(mouseY)))
        
        list.append(bolas, b)
    
def draw():
    background(0)
    
    for b in bolas:
        Bola.desenhar(b)
        
    for b in bolas:
        for c in bolas:
            if b != c and Bola.colide_com(b, c, 1 / frameRate):
                b.vel_x *= -b.elasticidade
                b.vel_y *= -b.elasticidade
                
                c.vel_x *= -c.elasticidade
                c.vel_y *= -c.elasticidade
        
        Bola.atualizar(b, 1 / frameRate)
