pontos = []
velocidade = 120

def setup():
    size(800, 600)
    smooth()
    
    for i in range(20):
        list.append(pontos, [random(width), random(height)])
    
def draw():
    if frameCount %  100 in [0, 1, 4, 5, 8, 9]:
        background(255)
    else:
        background(50)
    
    stroke(0, 220, 255)
    strokeWeight(5)
    for p in pontos:
        point(p[0], p[1])
        p[1] += velocidade / frameRate
        if p[1] > height:
            p[1] = 0
