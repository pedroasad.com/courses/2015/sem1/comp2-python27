"""Arte ASCII gerada por
    http://www.glassgiant.com/ascii/
"""

leao1 = """
=:,:,::,,:,,:,~~~:~~:,:::::~::~~:::,::,::::~~~~:~~=~~~~~::::::~~~:~~~==++???IIII
==,::,,,,,,,..::~::~:,::,~~:~~~~::~,,::,,,:~~==~~=~==~=~~:,,,,,~~~=====+=??I????
~~~~~,::,,,~,,:::,~,,::,::::~~~~:::~:,:,,::~~~====~~~===~~:::,::~~~~~==+????IIII
~::::::::,,:~:::::,,,:::::~~:~~~~:~:~::~::~~~~==+:+=+=++===::,,,:=~==+=++????III
~::,:,::,,:,,~+~~~,.,,,:,:~:~~::~:~:~=~:,,:=~====+++++~+++=~~:,,::~=~+++?+??IIII
:,,:,,,,,,:,,:,~~~:,::::=:~~:===:::::,::,,::====++?+++++=+=+~~~:,,:~==+?+?++?III
:,,,,,,,::::,,:=,,,::::~~~:~~+==~=~~::~:,~~:==+~==+?+=+++=?++==:~,,:===?+??????I
,,,,,,,,~:~=:::=~::,~~:::~~~==:=+=~~=::~:,:~~~=+~~==+=++?+++++=~=:,,:=+++??++??I
:::,,.::::::~=~,:::~,::~~~~=~~~:==~~~:~::,~~~:=~+~~~+===+?++?=~~~~,::,=+??+?++??
::,,,:~::==,~=~:~~~~:~:~~~=~~:::~~~~~:::,:~::=~=~+:~+=:===+?+?=+~==~,,:~+??+????
:,:~~=,~~~~:=~:~::,:~~==~~:=~::~=~~~~~::,::~:~::=~~++=++++=++?+++=~:~:,:,=?++???
~:~===:~~:~:==~:::~:~=:~:::~~,,:::::::,~:~=~~=+:=++~++:,,:,.~=?====~:,::,::+?=??
::===~~~===::~:::~:~::::~:,~~,:::::::~::~~~==+=~=?=..,,,..==+?++=+~~=:::::~~~+??
:==~~=:~~~::~,~:::~:,,.......,:::::::~==:~++~+=:=?~,.,,.,+?+=+++~===~=::~::~:~=+
:+~~=~~~=::::,,,:~:~:,~,..,...~=~~::~:+==:===?+~~+,:,:~~?+====+++:::~~=::=~:~~~=
:~=~~:~=:,,~:,,:,::~::~+=~:,..,~:,::~=~=~~=++=~=,=,:=??+::~~~~~=~=:::~~~~~~:=~=+
=~~==:~~:,~:,:,~:~~:~~:,~+=~=..~=~:::~=~:?=~?~:=+===?===~,~~====?+=:::::~~~~+===
===~:~~:=:,::::,:::::::::::==:,:~~:::~~~~:=~==+~==,~+++=::~+=====+:~:~::=~~====?
~~~:~~~:=::::~:,,::~~~==:::=~:,,~,,:,,::=+=::~:=~::=+~=+~=+~====+=::~:::~~~==+++
~~~::~~=~=:~,,::::,::::~~:~==::,::,:~:~~~~~~:+::~~=:~=?==~+~~=+++=::~==:~~~~+~++
,~,~,:~~~,::::,,,:,:,,:~,::,::,,~,:::::~~::,:~:::=+:+~=~++===?+++:,:::=~~~:~~~++
~==~~~~=:~:~~:,:,,:::~:::,:,:~,,:~:~~:~~=~.:~.:~~:+~::+~=+?+++=+~~:~~::~~==~====
=~:~,=~~~::~~~:,~:~::~:,,::,:~,::~::,~~:~=:~,~~~+==~~==+=+??+?+++:::::~~~~~=+~=+
===::~~~::~~~:,:,~~:::::::,:::,~:~:~::=,::~:::~:~:=~+=+~?+?I?+=+=~:::::~=~~===?+
~~====,~~~~::,~:=~~~:::::,::::::,:,::,,~~,::::~,,~~:=~==?=+???++=~~:::,::~~====+
:~~=+==~:~::::,,::~::~:~~,:~:::,:::,~:=~~::~:,==:=~~=:=~~=????++~:~::~::,~~~==++
~,~:~===::=:~~:..,:~:~~~~,:~=~,::,,,~:~~~==~~=:~==~=:==+=+=??=+=~,,~:~~~~~~===++
,:~~=~==~:~~:~~~,.:~~:~::,,:::::::~:~,:~==~===++=~+=?==+==+?====,:::::~~~=~=+?==
::~~~~===~==~~~~,,,:~:~::,,:~~:::~~~~=~~====~=+++~.,++++++++=+=,:,:::::~~~~=~+++
,~:~:::~=~::~~~::,..:~:::~:,::~~:.,..,:~~=~,,.,...~~==:?+??+++:,,::~,~:::~=+++++
,,,,::=~=~::=,::~:,.,~::,::~~~~,=:......,..,,.....~===~++?=I:?,,,,:::~:~~+=+~=++
...,::~:~+~~::~~~::..:~~:,==,:::~~~,.......,,,:~=+~~+=?+III?I~,:~:~~~~~~==~=~~=?
,,,,,:,::~=~~~~~:,:,..,~:~:=~=~~~~=~~:,......:+?++?==?=?+??I?I7:~+~:~+=~~~+~===+
,,:,,,,,:,~~~~.,,:,,,..,,,:~~~~=~==~====~..,:=++=+=?~+~+??I++,==:=~:~~===~===+~+
,,.,:,,:,~::::~:,:,,,..,:,::~=:~=+++=+=?+~.+~+??I++=???+IIIII+,,?~~=+::~=~~++=+?
,,,,,,..::::::=,~:,,.,,.~:~~~=~~:==+++?++,.=++IIIII?+I???I?I+=,,??==~+::~=~====+
,:,,,,.,,,,,:,::~~,,,,...~::=:~===?++?=+++.::=??IIIII??+??++?,,,:~=~:~~:~+++:===
,,,,,,::,::,,,,~~~:,,:,..,:::::~==+++=+:......~+=?I7?II?++===,,,,:==:~++~=++~~~+
,,.,:.,:,,,::,:::::::.,,,.,,:,,:==~~:,:~=:,,,~,,,:===:::,,:~,,,::,:?:::~~~=++=~+
..,.,,,.,,:,,,,,:~:~:::...,,......,,~~+=++~~=+==+??++?+~,~,:,,,:,::~~~,=~===+?~+
,.,,,..,,,,,:,,,,,:,~~~,,,,,....,:~=+++++?+++?+?I7II???I=:,::::,,==,:~~:::~+??==
,,,,,.,.,,,.,.,,,:.,,::~~,,,,,.,,~+==??++?=++?+???I?I??I?~:,::~,~~:::~~~:~:~++?+
,,,,,,,..:,,.,,:,:::,,::::,,,,,,,::~++?+++=?=+=??+I??=?=::::~::~,~=,:~~~=::=++++
:,,,,,,,,.,,,..,,,::,,,,~::,..,,.,,,~==~=~=?=?=?I+?=:,,+:,,:~:=,=:~:~~==~,~:=+++
,,,~,,:,,...:,,.,,..,:,.,:,::~~:,,,,::,:~,~=,~=,+~=,,,:~:::.:,~=~~~+:~====~,=+++
,,:,.,:.,,,,..,::::,,.,,,,,,:,,:,:.,,,,,,,,.:,,:+,:::~:,:,,,,~~~=~++:~:==?:::=++
,,,,,,,,,,,,.,...:,,,,..,,,,:,,..,,,,,,,,,,,..,,,,~:==~:=:,,===~==~==:,~=+=~,~=+
,,,,,,,,...:.,,.....,,....,,,,..,,,,,,,,,,,:,,,:,,,:~~=::,~::~+~?=~=:::~~+=+~:~~
:,,,,,,,,.,.,.,,..,:,,:,.,,,.,,::,,,.,::::::~..::,,,:~+~,~=~~~~+=:+:~~:~:===~==:
"""

leao2 = """
???????????????????+:::::::,,:,,,::,:~~~~,:::,::~~~:=:::,,,:::::::::=~~=~==~::::,,:~~~::~~~~=++???IIIIIIIIIIIIIIIIIIIIII
???????????????????===:::,,,,,,,,,,,~:~,,,::,:::::::~:,::,,,,:,:~~~~~~~~~=~~~~~:,,,:::~~~=~==++??I??II???IIIIIIIIIIIIIII
??????????????????+~=~~~::,:,.,~,.::~:,,,,,:::::~:~=::~:::::::::~~==:~==+++==:~~~::,,~~::~~~~++???IIIIIIIIIIIIIIIIIIIIII
????????????????====,::~:::::,,~:::,:~:,,,::::::::::::~::~::::~~~~~=+===+=+=+=~=~~:,,:::~~===++???IIIIIIIIIIIIIIIIIIIIII
????????????????=~==~::,:,::,:,,:~~~~~:,,:,,,::~::,:::~~~:::,,,~~~:==~++++?+++==+~~:,,,:~==+++++?????IIIIIIIIIIIIIIIIIII
?????????????+?=~~:~:,,,.,,,,,:,,:~~~~:,::,::::~~~~=~~~~~~,::,,:~~~===+???=+++++=~=~~,:,~~~=+?+????IIIIIIIIIIIIIIIIIIIII
?????????????++=~=~:,,,,,,,:,:::,:,:,:,::::~~~~=+=~~~+=~~::~:,::~==+~~~+=?+?+++=+=+=:~:,,~==+?????????IIIIIIIIIIIIIIIIII
??????????????+~==~:,,,,,.,,:,:~:,:~:,,,::~:~~:~~~~~~==~=~~:~:,:~~==+=~~~++~++???=++=~::,,~~++???++????I?IIIIIIIIIIIIIII
????????????+=::~::,,::,,,,:::,::~=::,::::,::~=~~~~~:~~=~~:=::,::~=+++~~:++==++?=+??+=~=~:,:=++?+???????IIIIIIIIIIIIIIII
???????????+==~::::::,,.,::::==~~~==,,:::~==~~=~=~::~==~~:~::,,::::==~=::+?~~==+?++++~==:~:::=+I????+?????IIIIIIIIIIIIII
???????????+=+~::::::::~~:.:~~=~+=~:::,~~:====:~==:,=~~~=~::,,,~:=:+:::=~+++=+=++==+?++=~~,:,::+?I++?????I??IIIIIIIIIIII
???????????===:::~::::~~=~~==:=::::::~::~:~::~::=~,::~::~~~:::::==?~=~.+===~:~:=~=~=++~+=~:~::,:~?+?+???+???IIIIIIIIIIII
??????????+===~~~::~,:===:~:+:=,~:=:~,~::~::~:~~~::::,:::~~::~:~~~~+~=?+.,..,,,.=~=+++=~==~::::~:~~+??????IIIIIIIIIIIIII
??????????+===~~~~=:::+~~~:=~=::::::~~~::,:,::::.,:~~:,:~:=:~~=++?+=~~I?:,,,,.,+?======~===~,::::~::=+????III?IIIIIIIIII
??????????+==~~~~~=~~====~~:~::::~:,:::::,:,..,,...~=::::~~~~~:===:=:=++:,,,:=??++==+?==~~:~=:~::~:~~=+??IIIIIIIIIIIIIII
?????????+===~::~~~~:===:~:~~:,::,:,,::~:,~==~.....:~~,~::=~==~~=~+:=++~~~=++?+:~=~=+===::::~~:~~=:===++??IIIIIIIIIIIIII
???I?????====:,,:~:~~~~~~:~:::,,::,:,,:::~~,~+==~:.,~:~:::~:~=:=+?+,:+~+:+?++~:==~=?+=~~~=~:~~~~:=====+??I?IIIIIIIIIIIII
I????????=:::::,::~==~+=~~::~~~:,:,,~:~~:~::::~+++,~==:::,::~~:~====+,===~~+~:~:==+=+=+~~~::~=~:~~~=?++???IIIIIIIIIIIIII
IIII?????==~:::,,,~+=~~+~:~~~~::,,:,:::~::~:+=:~:=:,:~:,:::=:~+~::~=+~~:~+:=~:~===~=+==+~,~~~:=~=~====????IIIIIIIIIIIIII
IIIII???+?~~::::,~:=:~~~~,:=~~~~:::,:,,~:~~:::~::=::.:,,::~~~::==~,::,=::~~+=+~~==+++=+:,,:~::~~====+++????IIIIIIIIIIIII
IIIII??+++==:~:::=~~~:~,,:=+:::::,,,,,,,,:::::,,,:~,,::~:~,:::=~=::~~~~~~=::~?=~=~=+++=::::~=~::~+~==+?????IIIIIIIIIIIII
III??I?+=~~~::,,,~==~~:~,~=:=::::::,,,::::::,::,::,,,~~:::::~:,::.~=:~++~,==+==+==~+~+==:::::====~~===???I?IIIIIIIIIIIII
IIII???+=~:,::,,::===:~~+~:~::~~~~,::,,::::::,:::::,::~,:~~~:~~:~:::~~~~::~++=++?++++++~::,::~=~+=+==++???I??IIIIIIIIIII
II?I??++=~:,~~,,,,~=+==~=:~~:~:~=:::::~:::~:,:::::,,~~~:~,~~=:=,,:~~~~~==+,~=+=???++++=::::::~~~+===++?????II?IIIIIIIIII
III?I??==~::::,:,,,:==+=~~:=~:~~~,::~:::~::,,:::::,::~:,:::~:~:~,~:,~~:~=~~?~=??+I?++==~,,::::~=~==+++??I????IIII?IIIIII
III???+=~:~:~:,~,,,:~==~=+~~=~~:~::,::~,~~::~:.:::::::~~,::~,:~:::.~~~=:~~=+?~I++???++=~::=:::~~~~+=++???I?I??IIIIIIIIII
?I????==~:~:::,,:.,,:,~~=+==~~:~~:,,,::~:~~~~~,::::::::::~:::~:~~~~:~:~~~~~~+++=???=~=~:::~:::~~~~~=++???II?I?IIIIIIIIII
II?II+===::~:=:,,,.,:~,~~==~~~~~:~:,,,:~~~~~::::::::~:::~:,~:~~=~~=+:~,~++:~~=+~+?+=+=:,,~~:=~::~====+????II????I??IIIII
IIII?=~=~=,~~,~:.::,,:=~~=~=+~~==~~~:,,~~::~::~,:,::~:::~==~=:===:=++++=?===+=~=++===:,,::~::~=~~==?=+=?+????II??I???III
????=~~~~:~::~,::..,,,:=~~~+=~~~:~:~::,,~~::::,::~:::,~,~~~:~~+=~~=+~~~.,++=+++??=++~,:,:::::~~~~++=?++??I?I?I?I?????III
????~=:~:~::::,,.:::,,,::=~=+,~:=~::~:,.,~~:,:::,~:~~:.,....:,=:,,,,...,~+~==+??==++:,,,::::~~~+===++=+?+??III??I+??I?II
I??+~=,=:,:.,:::,,.,.,,,:~~:===:=::::::..~~:,:,:~:~~:=:......,.....,,.,~+=:=+?=II??,,,~:,:~=:==~~==+=???????III?I?????II
I??=~~~~:,:~,:,,::,,,..,,,:,~==~,~~:::,,..~:,,~==~::,=~~:,......,,,:~+:++=?+??I??I?,:::~:::~~~==~~==+=+???I????I???????I
??++~~:::,.,,::,.,,,,:,.,,,:::=:=~:::,,,,.:,::::~=======~~~~,.....=?I??=+=+?+?++?II=+=:::~~=~===~=++~++=?????+???????II?
I???==~,,:..,,:~:~,..,~:,,,,,,:~,=,,,:::,,,.::,~,:~~~::::~~==~..:+==???+?++??????II??I::~:~~=~+~+~==+?+++?I??I?I??????II
III?+~:,,,:..,,,,:,:,,,,,~,,,~,:::~,,::::.,.::,:======+==+++++=.=+=??II?==++?????+:=~:?~==+:::+~~?==+++=++?II???I+??????
I?I+=~:,,,,,..,,,:.,,,,,,...,:,:::~=:,,,,,,:.~~~~:~~~~++++??++=.:++?IIIII?+?+??III~==:~?~~++:~~+==~=~=?++???II?++???????
II?+=~::,,,,,,.,.,,,,:,,,,.,:,,,::::~,,,,,,..,:::~:+=++??+=++=~.,++++IIIII?I???+=?~,..::=++:::=+=++~=+??????????==?+???+
III+==~::,,,,,,,..,,.,,,,,,~,,:,.,:,:~~:,,.,..,:,:::~+++====.,,....:++???II?I++~+=::,,,::~:==+~==+?~=~++??????+++~=+++??
II?++==~::::,,:.,,.,,..::,,.,.:,~,,:::~~~,,,....,.,,~=~:~,,~,=:,,:,::,,:=+=~::=,::,,,,,,~+,~~++=+++====++??++?+++++=+++?
?II?+?+=~:::,,,,,,,,..,.,,,.,,:,,~,:::~::~~..,.....,...,,~=+=++~~++:=+?????++::::,,,,~:::~~~,=~===++=+~+=??=~?++~++~+++?
III?I???=~::::,::,,,..,.,.,,,,.,,,:,,,::::~~,,,:....,,:~++=+++?+?+?+I?IIIIII?+~,:~:,:~:=:,~~=:::~+++==?~++??+++:=~=~=+++
II??II???==~~:,,,::,,,:,....,,...,,,:,,,,:~::,:,,,..,,~+==~?+=+=?++?+?I?III?I+,::~:::::~:::~=~:~:~+++==?==++=~===+=~~~++
IIIIIII??====:::,:,,,,,,,,...:,..,.,,,:,,,,:,:,,,,,.,,::~++?+++~++?I??+?II=,=,~.,::=,::~~,~~~~=::==+++::++====:~~=~~~=++
IIIIIIIII??+~=~:,:::,,:,.,.,,.,,:..,,,,~,.,,:::,,.,,,..,:~==~=~=++++??++?~:~:,:,,=~~~:~:~:~~==~,::=+++~:~+=~:::::~~~=+~=
IIIIIIII?I?+==~~::,,,,,~,,,,,.,.:....,:,.~,,.,,,:~~~:.,,,:,,:~:~~,:=+=++:,,:,:::,~::~~~~++,~==+=,,~=++~=~~===,~::~~=+===
IIIIIIIIII++?==~::,:,,,,:,,,,,,,.,,:,:,:,...,,,~:.,,,,,..,.,,,.:.,,~~:~=,,:~:,:,,,,~=+=~++:~:=+?~:~~+?~=~~::,,:~:~~~==+=
IIIIIIIII?I?+?+~~~:,,,,,:,:,,,:,.,..,,,,:,....:,,,,..,,,,,,,,,,,,,,.:~.,,:~~:=::,:~=+:+=~==::~?++~,:~+?=~~~:~:,::~~~~=++
IIIIIIIIII?????~::,,,,:,,,:,...:..,.,.,,,:...,,,,,,.,..,,,,,,,,::,,,:,.,:~~~~~:::=~===+=+=~::~:+=+=:::=+=~~~:,,::,~::==+
IIIIIIIIIII????=~~:,,,,,,,,,,.,.,.,.,....,,,..,:,,,.,,,,,.::::,:::,.::,,:,::==,~:~===?+:=::::~,=:=~+::~:~=:~=~::~::~::~=
IIIIIIIIIIIIII?:~~~:::,,,,,,,,,,,..,..,,,:,..,,,,,,,:,..::::::::~:,.,,.:,:::=+~::~====+::::~::,,~=~::::~=,,~=~==:~:~:~~=
IIIIIIIIIIIIIII::~~:::,,,::,,,,,..,,..,.,,,,,..,,,,,,,,.,,,:::~:~~~:,::,,,:,:=~:=:=~::::~:,~,:~~::~::~::~:::~==?=:=~=~:=
IIIIIIIIIIIIII=,~=~=::,,,,..,,,,...,.,,.,....,,,,,.,,,:,,,:~=~:::=~,:,:,,,:::::~+:~::~:::~,~:,,~=:~=::=:+~~~,:~=+:=+~~=~
"""

leao = """
???????????????????+:::::::,,:,,,::,:~~~~,:::,::~~~:=:::,,,:::::::::=~~=~==~::::,,:~~~::~~~~=++???IIIIIIIIIIIIIIIIIIIIII
???????????????????===:::,,,,,,,,,,,~:~,,,::,:::::::~:,::,,,,:,:~~~~~~~~~=~~~~~:,,,:::~~~=~==++??I??II???IIIIIIIIIIIIIII
??????????????????+~=~~~::,:,.,~,.::~:,,,,,:::::~:~=::~:::::::::~~==:~==+++==:~~~::,,~~::~~~~++???IIIIIIIIIIIIIIIIIIIIII
????????????????====,::~:::::,,~:::,:~:,,,::::::::::::~::~::::~~~~~=+===+=+=+=~=~~:,,:::~~===++???IIIIIIIIIIIIIIIIIIIIII
????????????????=~==~::,:,::,:,,:~~~~~:,,:,,,::~::,:::~~~:::,,,~~~:==~++++?+++==+~~:,,,:~==+++++?????IIIIIIIIIIIIIIIIIII
?????????????+?=~~:~:,,,.,,,,,:,,:~~~~:,::,::::~~~~=~~~~~~,::,,:~~~===+???=+++++=~=~~,:,~~~=+?+????IIIIIIIIIIIIIIIIIIIII
?????????????++=~=~:,,,,,,,:,:::,:,:,:,::::~~~~=+=~~~+=~~::~:,::~==+~~~+=?+?+++=+=+=:~:,,~==+?????????IIIIIIIIIIIIIIIIII
??????????????+~==~:,,,,,.,,:,:~:,:~:,,,::~:~~:~~~~~~==~=~~:~:,:~~==+=~~~++~++???=++=~::,,~~++???++????I?IIIIIIIIIIIIIII
????????????+=::~::,,::,,,,:::,::~=::,::::,::~=~~~~~:~~=~~:=::,::~=+++~~:++==++?=+??+=~=~:,:=++?+???????IIIIIIIIIIIIIIII
???????????+==~::::::,,.,::::==~~~==,,:::~==~~=~=~::~==~~:~::,,::::==~=::+?~~==+?++++~==:~:::=+I????+?????IIIIIIIIIIIIII
???????????+=+~::::::::~~:.:~~=~+=~:::,~~:====:~==:,=~~~=~::,,,~:=:+:::=~+++=+=++==+?++=~~,:,::+?I++?????I??IIIIIIIIIIII
???????????===:::~::::~~=~~==:=::::::~::~:~::~::=~,::~::~~~:::::==?~=~.+===~:~:=~=~=++~+=~:~::,:~?+?+???+???IIIIIIIIIIII
??????????+===~~~::~,:===:~:+:=,~:=:~,~::~::~:~~~::::,:::~~::~:~~~~+~=?+.,..,,,.=~=+++=~==~::::~:~~+??????IIIIIIIIIIIIII
??????????+===~~~~=:::+~~~:=~=::::::~~~::,:,::::.,:~~:,:~:=:~~=++?+=~~I?:,,,,.,+?======~===~,::::~::=+????III?IIIIIIIIII
??????????+==~~~~~=~~====~~:~::::~:,:::::,:,..,,...~=::::~~~~~:===:=:=++:,,,:=??++==+?==~~:~=:~::~:~~=+??IIIIIIIIIIIIIII
?????????+===~::~~~~:===:~:~~:,::,:,,::~:,~==~.....:~~,~::=~==~~=~+:=++~~~=++?+:~=~=+===::::~~:~~=:===++??IIIIIIIIIIIIII
???I?????====:,,:~:~~~~~~:~:::,,::,:,,:::~~,~+==~:.,~:~:::~:~=:=+?+,:+~+:+?++~:==~=?+=~~~=~:~~~~:=====+??I?IIIIIIIIIIIII
I????????=:::::,::~==~+=~~::~~~:,:,,~:~~:~::::~+++,~==:::,::~~:~====+,===~~+~:~:==+=+=+~~~::~=~:~~~=?++???IIIIIIIIIIIIII
IIII?????==~:::,,,~+=~~+~:~~~~::,,:,:::~::~:+=:~:=:,:~:,:::=:~+~::~=+~~:~+:=~:~===~=+==+~,~~~:=~=~====????IIIIIIIIIIIIII
IIIII???+?~~::::,~:=:~~~~,:=~~~~:::,:,,~:~~:::~::=::.:,,::~~~::==~,::,=::~~+=+~~==+++=+:,,:~::~~====+++????IIIIIIIIIIIII
IIIII??+++==:~:::=~~~:~,,:=+:::::,,,,,,,,:::::,,,:~,,::~:~,:::=~=::~~~~~~=::~?=~=~=+++=::::~=~::~+~==+?????IIIIIIIIIIIII
III??I?+=~~~::,,,~==~~:~,~=:=::::::,,,::::::,::,::,,,~~:::::~:,::.~=:~++~,==+==+==~+~+==:::::====~~===???I?IIIIIIIIIIIII
IIII???+=~:,::,,::===:~~+~:~::~~~~,::,,::::::,:::::,::~,:~~~:~~:~:::~~~~::~++=++?++++++~::,::~=~+=+==++???I??IIIIIIIIIII
II?I??++=~:,~~,,,,~=+==~=:~~:~:~=:::::~:::~:,:::::,,~~~:~,~~=:=,,:~~~~~==+,~=+=???++++=::::::~~~+===++?????II?IIIIIIIIII
III?I??==~::::,:,,,:==+=~~:=~:~~~,::~:::~::,,:::::,::~:,:::~:~:~,~:,~~:~=~~?~=??+I?++==~,,::::~=~==+++??I????IIII?IIIIII
III???+=~:~:~:,~,,,:~==~=+~~=~~:~::,::~,~~::~:.:::::::~~,::~,:~:::.~~~=:~~=+?~I++???++=~::=:::~~~~+=++???I?I??IIIIIIIIII
?I????==~:~:::,,:.,,:,~~=+==~~:~~:,,,::~:~~~~~,::::::::::~:::~:~~~~:~:~~~~~~+++=???=~=~:::~:::~~~~~=++???II?I?IIIIIIIIII
II?II+===::~:=:,,,.,:~,~~==~~~~~:~:,,,:~~~~~::::::::~:::~:,~:~~=~~=+:~,~++:~~=+~+?+=+=:,,~~:=~::~====+????II????I??IIIII
IIII?=~=~=,~~,~:.::,,:=~~=~=+~~==~~~:,,~~::~::~,:,::~:::~==~=:===:=++++=?===+=~=++===:,,::~::~=~~==?=+=?+????II??I???III
????=~~~~:~::~,::..,,,:=~~~+=~~~:~:~::,,~~::::,::~:::,~,~~~:~~+=~~=+~~~.,++=+++??=++~,:,:::::~~~~++=?++??I?I?I?I?????III
????~=:~:~::::,,.:::,,,::=~=+,~:=~::~:,.,~~:,:::,~:~~:.,....:,=:,,,,...,~+~==+??==++:,,,::::~~~+===++=+?+??III??I+??I?II
I??+~=,=:,:.,:::,,.,.,,,:~~:===:=::::::..~~:,:,:~:~~:=:......,.....,,.,~+=:=+?=II??,,,~:,:~=:==~~==+=???????III?I?????II
I??=~~~~:,:~,:,,::,,,..,,,:,~==~,~~:::,,..~:,,~==~::,=~~:,......,,,:~+:++=?+??I??I?,:::~:::~~~==~~==+=+???I????I???????I
??++~~:::,.,,::,.,,,,:,.,,,:::=:=~:::,,,,.:,::::~=======~~~~,.....=?I??=+=+?+?++?II=+=:::~~=~===~=++~++=?????+???????II?
I???==~,,:..,,:~:~,..,~:,,,,,,:~,=,,,:::,,,.::,~,:~~~::::~~==~..:+==???+?++??????II??I::~:~~=~+~+~==+?+++?I??I?I??????II
III?+~:,,,:..,,,,:,:,,,,,~,,,~,:::~,,::::.,.::,:======+==+++++=.=+=??II?==++?????+:=~:?~==+:::+~~?==+++=++?II???I+??????
I?I+=~:,,,,,..,,,:.,,,,,,...,:,:::~=:,,,,,,:.~~~~:~~~~++++??++=.:++?IIIII?+?+??III~==:~?~~++:~~+==~=~=?++???II?++???????
II?+=~::,,,,,,.,.,,,,:,,,,.,:,,,::::~,,,,,,..,:::~:+=++??+=++=~.,++++IIIII?I???+=?~,..::=++:::=+=++~=+??????????==?+???+
III+==~::,,,,,,,..,,.,,,,,,~,,:,.,:,:~~:,,.,..,:,:::~+++====.,,....:++???II?I++~+=::,,,::~:==+~==+?~=~++??????+++~=+++??
II?++==~::::,,:.,,.,,..::,,.,.:,~,,:::~~~,,,....,.,,~=~:~,,~,=:,,:,::,,:=+=~::=,::,,,,,,~+,~~++=+++====++??++?+++++=+++?
?II?+?+=~:::,,,,,,,,..,.,,,.,,:,,~,:::~::~~..,.....,...,,~=+=++~~++:=+?????++::::,,,,~:::~~~,=~===++=+~+=??=~?++~++~+++?
III?I???=~::::,::,,,..,.,.,,,,.,,,:,,,::::~~,,,:....,,:~++=+++?+?+?+I?IIIIII?+~,:~:,:~:=:,~~=:::~+++==?~++??+++:=~=~=+++
II??II???==~~:,,,::,,,:,....,,...,,,:,,,,:~::,:,,,..,,~+==~?+=+=?++?+?I?III?I+,::~:::::~:::~=~:~:~+++==?==++=~===+=~~~++
IIIIIII??====:::,:,,,,,,,,...:,..,.,,,:,,,,:,:,,,,,.,,::~++?+++~++?I??+?II=,=,~.,::=,::~~,~~~~=::==+++::++====:~~=~~~=++
IIIIIIIII??+~=~:,:::,,:,.,.,,.,,:..,,,,~,.,,:::,,.,,,..,:~==~=~=++++??++?~:~:,:,,=~~~:~:~:~~==~,::=+++~:~+=~:::::~~~=+~=
IIIIIIII?I?+==~~::,,,,,~,,,,,.,.:....,:,.~,,.,,,:~~~:.,,,:,,:~:~~,:=+=++:,,:,:::,~::~~~~++,~==+=,,~=++~=~~===,~::~~=+===
IIIIIIIIII++?==~::,:,,,,:,,,,,,,.,,:,:,:,...,,,~:.,,,,,..,.,,,.:.,,~~:~=,,:~:,:,,,,~=+=~++:~:=+?~:~~+?~=~~::,,:~:~~~==+=
IIIIIIIII?I?+?+~~~:,,,,,:,:,,,:,.,..,,,,:,....:,,,,..,,,,,,,,,,,,,,.:~.,,:~~:=::,:~=+:+=~==::~?++~,:~+?=~~~:~:,::~~~~=++
IIIIIIIIII?????~::,,,,:,,,:,...:..,.,.,,,:...,,,,,,.,..,,,,,,,,::,,,:,.,:~~~~~:::=~===+=+=~::~:+=+=:::=+=~~~:,,::,~::==+
IIIIIIIIIII????=~~:,,,,,,,,,,.,.,.,.,....,,,..,:,,,.,,,,,.::::,:::,.::,,:,::==,~:~===?+:=::::~,=:=~+::~:~=:~=~::~::~::~=
IIIIIIIIIIIIII?:~~~:::,,,,,,,,,,,..,..,,,:,..,,,,,,,:,..::::::::~:,.,,.:,:::=+~::~====+::::~::,,~=~::::~=,,~=~==:~:~:~~=
IIIIIIIIIIIIIII::~~:::,,,::,,,,,..,,..,.,,,,,..,,,,,,,,.,,,:::~:~~~:,::,,,:,:=~:=:=~::::~:,~,:~~::~::~::~:::~==?=:=~=~:=
IIIIIIIIIIIIII=,~=~=::,,,,..,,,,...,.,,.,....,,,,,.,,,:,,,:~=~:::=~,:,:,,,:::::~+:~::~:::~,~:,,~=:~=::=:+~~~,:~=+:=+~~=~
"""
