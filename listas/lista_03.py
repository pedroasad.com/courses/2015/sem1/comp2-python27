import math

class Paraquedista(object):
    def __init__(self, pos, vel = 0.0):
        self.pos = pos
        self.vel = vel
        self.aberto = False

    def __repr__(self):
        return 'Paraquedista(pos = %s, vel = %s, aberto = %d)' % (repr(self.pos), repr(self.vel), self.aberto)

    def abrir_paraquedas(self):
        self.aberto = True

	def atualizar(self, dt):
		if self.aberto:
			k = 10.0
		else:
			k = 0.18

		vel = self.vel                 +           (self.vel + 10 / k) * (math.exp(-k * dt) - 1)
		pos = self.pos - (10 / k) * dt - (1 / k) * (self.vel + 10 / k) * (math.exp(-k * dt) - 1)

		self.vel = vel
		self.pos = pose

class Carteira(dict):
	def __init__(self, moedas = {}):
		dict.__init__(self, { 5 : 0, 10 : 0, 25 : 0, 50 : 0, 100 : 0 })
		self.update(moedas)

	def total(self):
		#return sum([v * q for q, v in self.items()]
		valor = 0
		for v, q in self.items():
			valor += v * q
		return valor

	def troco(self, carteira, valor):
		dif = valor - carteira.total()
		moedas = {}

		for v in sorted(self.keys()):
			q = self[v]
			moedas[v] = min(q, dif / q)
			dif -= v * moedas[v]

		if dif > 0:
			return None
		else:
			return Carteira(moedas)

	def dar_troco(self, carteira, valor):
		self += carteira
		troco = self.troco(carteira, valor)
		self -= troco
		return troco

	def __add__(self, carteira):
		moedas = self.copy()

		for v, q in carteira.items():
			moedas[v] += q

		return moedas

	def __iadd__(self, carteira):
		for v, q in carteira.items():
			self[v] += q

	def __isub__(self, carteira):
		for v, q in carteira.items():
			self[v] -= q
		
	def __sub__(self, carteira):
		moedas = self.copy()

		for v, q in carteira.items():
			moedas[v] += q

		return moedas

class Petisqueira(object):
	def __init__(self):
		self.produtos = []
		self.estoque = []
		self.precos = []
		self.saldo = Carteira({ 100 : 2, 50 : 4, 25 : 8, 10 : 20, 5 : 40 })

	def compra(self, produto, valor_pago):
		i = self.produtos.index(produto)
		if self.estoque[i] > 0 and valor_pago >= self.precos[i]:
			troco = self.saldo.dar_troco(valor_pago, self.precos[i])
			if troco:
				self.estoque[i] -= 1
				return (True, troco)
			else:
				return (False, valor_pago)
		else:
			return (False, valor_pago)
