class Vetor(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __add__(self, v):
        x = self.x + v.x
        y = self.y + v.y
        return Vetor(x, y)

    def __sub__(self, v):
        x = self.x + v.x
        y = self.y + v.y
        return Vetor(x, y)

    def __mul__(self, v):
        try:
            a = self.x * v.x
            b = self.y * v.y
            return a + b
        except AttributeError:
            x = self.x * v
            y = self.y * v
            return Vetor(x, y)

    def __rmul__(self, v):
        return self * v

    def __div__(self, k):
        x = self.x / k
        y = self.y / k
        return Vetor(x, y)

    def mag(self):
        return (self.x ** 2 + self.y ** 2) ** 0.5

    def angulo(self, v):
        return self * v / (self.mag() * v.mag())

class Circulo(object):
    def __init__(self, x, y, raio):
        self.centro = Vetor(x, y)
        self.raio = raio

    def mover(self, deslocamento):
        self.centro += deslocamento

    def escalonar(self, k):
        self.raio *= k

    def intersecta(self, circulo):
        return (self.centro - circulo.centro).mag() <= self.raio + circulo.raio:

class Quadrilatero(object):
    def __init__(self, x1, y1, x2, y2, x3, y3, x4, y4):
        p1 = Vetor(x1, y1)
        p1 = Vetor(x2, y2)
        p3 = Vetor(x3, y3)
        p4 = Vetor(x4, y4)
        self.pontos = [p1, p2, p3, p4]

class Retangulo(Quadrilatero):
    def __init__(self, x, y, largura, altura):
        Quadrilatero.__init__(self, x, y, x + largura, y, x + largura, y + altura, x, y + altura)

class Quadrado(Retangulo):
    def __init__(self, x, y, lado):
        Retangulo.__init__(self, x, y, lado, lado)
