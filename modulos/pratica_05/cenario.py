# coding: utf-8
"""Conversões entre unidades"""

escala = 400 # 1 metro == 400 pixels
fps = 60.0

def pixels(m):
    """Converte `m` metros para o comprimento correspondente, em pixels."""
    return m * escala

def metros(p):
    """Converte `p` pixels para o comprimento correspondente, em metros."""
    return p / escala

def frames(s):
    """Converte `s` segundos para a duração correspondente, em quadros (frames)."""
    return s * frameRate

def segundos(f):
    """Converte `f` quadros (frames) para a duração correspondente, em segundos."""
    return f / frameRate
    
