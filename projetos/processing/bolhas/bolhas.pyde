bolhas = []

def setup():
    size(800, 600)
    
    global bolhas
    bolhas = [[random(width), random(height), random(100), random(256)] for i in range(50)]
    
def draw():
    background(50, 150, 255)
    
    noFill()
    for b in bolhas:
        stroke(100, 225, 255, b[3])
        ellipse(b[0], b[1], b[2], b[2])
        b[2] += 1
        b[3] -= 2
        if b[3] < 0:
            b[0] = random(width)
            b[1] = random(width)
            b[2] = random(5)
            b[3] = 255
