import cenario
    
class Bola:
    velocidade_ejecao = 15.0
    
    def __init__(self, nome, raio, cor, pos, vel = (0.0, 0.0), elasticidade = 0.85):
        self.nome = nome
        self.raio = raio
        self.cor = cor
        self.pos_x, self.pos_y = pos
        self.vel_x, self.vel_y = vel
        self.elasticidade = elasticidade
        
    def atualizar(self, delta_tempo):
        self.pos_x += self.vel_x * delta_tempo
        self.pos_y += self.vel_y * delta_tempo
        
        if self.pos_x < self.raio:
            self.pos_x = self.raio
            self.vel_x *= -self.elasticidade
            
        if self.pos_x >= cenario.largura - self.raio:
            self.pos_x = cenario.largura - self.raio
            self.vel_x *= -self.elasticidade
        
        if self.pos_y >= cenario.altura - self.raio:
            self.pos_y = cenario.altura - self.raio
            self.vel_y *= -self.elasticidade
        
        self.vel_y += cenario.gravidade * delta_tempo
        
    def ao_redor_de(self, x, y):
        return (x - self.pos_x)**2 + (y - self.pos_y)**2 <= self.raio**2
    
    def colide_com(self, outra_bola, delta_tempo):
        delta_tempo *= 1.2
        return ((self.pos_x + self.vel_x * delta_tempo) - (outra_bola.pos_x + outra_bola.vel_x  * delta_tempo))**2 + ((self.pos_y + self.vel_y * delta_tempo) - (outra_bola.pos_y + outra_bola.vel_y * delta_tempo))**2 <= (self.raio + outra_bola.raio)**2
        
    def desenhar(self):
        noStroke()
        fill(self.cor[0], self.cor[1], self.cor[2], self.cor[3])
        
        x = cenario.pixels(self.pos_x)
        y = cenario.pixels(self.pos_y)
        r = cenario.pixels(self.raio)
        
        textSize(18)
        text(self.nome, x - r, y - r)
        
        ellipseMode(CENTER)
        ellipse(x, y, 2 * r, 2 * r)
        
    def explodir(self, n):
        novo_volume = Bola.volume(self) / n
        novo_raio = (3 * novo_volume / (4 * PI)) ** (1.0/3)
        
        novas_bolas = []
        for i in range(n):
            nome = self.nome + '.%d' % (i+1)
            vel_x = self.vel_x / n + Bola.velocidade_ejecao * cos(i * TWO_PI / n)
            vel_y = self.vel_y / n + Bola.velocidade_ejecao * sin(i * TWO_PI / n)
            pos_x = self.pos_x + vel_x
            pos_y = self.pos_y + vel_y
            nova_bola = Bola(nome, novo_raio, self.cor, (pos_x, pos_y), (vel_x, vel_y), self.elasticidade)
            list.append(novas_bolas, nova_bola)
            
        return novas_bolas
        
    def volume(self):
        return 4 * PI * self.raio**3 / 3
