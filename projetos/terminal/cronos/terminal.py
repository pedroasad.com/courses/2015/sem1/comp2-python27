# coding:utf-8 #

from cronos import *
import pickle
import sys
import pdb
import textwrap

def buscarPorNome():
	nome = raw_input('Nome completo (ou parte) da atividade: ')
	nome = str.lower(nome)
	
	resultado = []
	pdb.set_trace()
	for a in atividades:
		if nome in str.lower(a.titulo):
			resultado += [str.lower(a.titulo)]

	resultado = sorted(resultado)
	resultado = str.join(', ', resultado)

	# Se o módulo textwrap não estiver instalado, basta excluir estas duas linhas
	resultado = textwrap.wrap(resultado, 80)
	resultado = str.join('\n', resultado)

	print resultado

def encerrarPrograma():
	resposta = raw_input('Deseja salvar suas alterações? [s/n]')
	resposta = str.lower(resposta)

	if resposta in 'sim':
		pickle.dump(atividades, file('atividades.dat', 'w'))
		print 'Alterações salvas!'

	print 'Tchau...'
	sys.exit(0)

def imprimirAjuda():
	print """
================================================================================
Cronos - ajuda

--------------------------------------------------------------------------------
RESUMO

Cronos é um programa para gerenciamento inteligente de tempo. Descubra os seus
gargalos, corte o inútil, reduza o demorado, enfim, desfrute do seu tempo!

--------------------------------------------------------------------------------
COMANDOS

Comandos são insensíveis a caixa alta. Abaixo, segue uma lista dos comandos
disponíveis.

agenda		Exibe o planejamento horário da semana

ajuda		Exibe este texto de ajuda

carga		Contabiliza o tempo livre e ocioso por semana

busca		Busca uma atividade por nome dentro da sua agenda

grupos		Lista todos os grupos de atividades definidos até o momento por você

listar		Mostra todas as atividades atualmente programadas em sua agenda

nova		Cria uma nova atividade

sair		Oferece a chance de salvar suas alterações e depois encerra o
			programa
"""

def imprimirBoasVindas():
	print """
==================================
Cronos 0.1 (2015)
Gerenciamento inteligente de tempo
----------------------------------
* Digite 'sair' + Enter para sair do programa
* Digite 'ajuda' + Enter para pedir ajuda
	"""

def iniciarPrograma():
	global atividades

	try:
		atividades = pickle.load(file('atividades.dat'))
	except IOError:
		print 'Alerta: Não existe agenda prévia, ou não consigo carregá-la. Criarei uma nova'
		atividades = []

def listarAtividades():
	for a in atividades:
		print a

def listarGrupos():
	grupos = []
	for a in atividades:
		grupos += a.categorias

	grupos = set(grupos)
	grupos = sorted(grupos)
	grupos = str.join(', ', grupos)

	# Se o módulo textwrap não estiver instalado, basta excluir estas duas linhas
	grupos = textwrap.wrap(grupos, 80)
	grupos = str.join('\n', grupos)

	print grupos

# Insira novos comandos aqui
comandos = {
	'agenda': None,
	'ajuda' : imprimirAjuda,
	'busca' : buscarPorNome,
	'carga' : None,
	'grupos': listarGrupos,
	'listar': listarAtividades,
	'nova'  : None,
	'sair'  : encerrarPrograma,
}

def principal():
	while True:
		c = raw_input('Cronos> ')
		c = str.lower(c)

		if c not in comandos:
			print 'Erro: comando "' + c + '" não reconhecido!'
		elif comandos[c] is None:
			print 'Opa, essa função ainda não foi implementada...'
			print 'MÃOS À OBRA!'
		else:
			comandos[c]()

# Este código abaixo é executado comente quando este módulo (arquivo) é
# executado, ou seja, usado como programa. No caso de um comando
#
# 	import terminal
#
# Este código não será executado.
if __name__ == '__main__':
	imprimirBoasVindas()
	iniciarPrograma()
	try:
		principal()
	except EOFError:
		encerrarPrograma()
